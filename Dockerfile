FROM centos:7

RUN  yum install -y python3 python3-pip
RUN pip3 install flask flask-jsonpify flask-restful


WORKDIR /python-api
COPY python-api.py /python-api
EXPOSE 5290
CMD python3 /python-api/python-api.py
